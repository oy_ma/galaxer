﻿using UnityEngine;
using System.Collections;

public class TextBoxTrigger : MonoBehaviour
{
    public int triggerId;
    private TextBox textBox;

    // Use this for initialization
    void Start()
    {
        textBox = GetComponentInParent<TextBox>(); 
    }

    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.gameObject.tag == "Player")
        {
            textBox.ShowBox(triggerId);
            Destroy(gameObject);
        }     
    }   
}
