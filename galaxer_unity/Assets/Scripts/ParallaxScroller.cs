﻿using UnityEngine;
using System.Collections;

public class ParallaxScroller : MonoBehaviour {

    public Transform[] backgrounds;
    private float[] parallaxAmount;
    public float smoothness = 1f;

    private Transform cam;
    private Vector3 prevCameraPos;

    void Awake()
    {
        cam = Camera.main.transform;
    }

	// Use this for initialization
	void Start ()
    {
        prevCameraPos = cam.position;
        parallaxAmount = new float[backgrounds.Length];

        for (int i = 0; i < backgrounds.Length; i++)
        {
            parallaxAmount[i] = backgrounds[i].position.z * -1;
        }	
	}
	
	// Update is called once per frame
	void Update ()
    {
        for (int i = 0; i < backgrounds.Length; i++)
        {
            float parallax = (prevCameraPos.x - cam.position.x) * parallaxAmount[i];
            float parallaxy = (prevCameraPos.y - cam.position.y) * parallaxAmount[i];
            float backgroundTargetPosX = backgrounds[i].position.x + parallax;
            float backgroundTargetPosY = backgrounds[i].position.y + parallaxy;
            Vector3 backgroundTargetPos = new Vector3(backgroundTargetPosX, backgroundTargetPosY, backgrounds[i].position.z);
            backgrounds[i].position = Vector3.Lerp(backgrounds[i].position, backgroundTargetPos, smoothness * Time.deltaTime);
        }

        prevCameraPos = cam.position;
	
	}
}
