﻿using UnityEngine;
using System.Collections;

public class EnemyRed : MonoBehaviour {

    private Player player;
    private float chaseDist = 2.5f;
    private float moveSpeed = 2f;
    private bool isChasing;

	// Use this for initialization
	void Start () {

        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        isChasing = false;
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Vector3.Distance(transform.position, player.transform.position) <= chaseDist && !isChasing)
        {
            Debug.Log("in range");
            Destroy(this.GetComponent("SplineController"));
            Destroy(this.GetComponent("SplineInterpolator"));
            isChasing = true;
        }

        if (isChasing)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, moveSpeed * Time.deltaTime);
        }
	}

    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.gameObject.tag == "Bullet")
        {
            Debug.Log("enemy got shot");
            Destroy(gameObject);
        } else if (target.gameObject.tag == "Player")
        {
            Debug.Log("player got killed");
            player.isAlive = false;
        }
    }
}
