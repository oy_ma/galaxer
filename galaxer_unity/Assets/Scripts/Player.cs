﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{

    public float speed;
    public Vector2 maxVelocity;
    public float maxX, minY;
    private float forceX, forceY;

    public bool isControllable;   
    public bool isAlive;
    private bool facingRight;

    private Rigidbody2D rb2d;
    private PlayerController controller;
    private Animator anim;

    public GameObject bullet;
    public float bulletSpeed = 10f;

  
    // Use this for initialization
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        controller = GetComponent<PlayerController>();
        anim = GetComponent<Animator>();
        isAlive = true;
        isControllable = true;
        facingRight = true;       
    }

    // Update is called once per frame
    void Update()
    {
        //forceX = Input.GetAxis("Horizontal");
        //forceY = Input.GetAxis("Vertical");

        forceX = 0;
        forceY = 0;

        if (isControllable)
        {
            float absVelX = Mathf.Abs(rb2d.velocity.x);
            float absVelY = Mathf.Abs(rb2d.velocity.y);
            if (controller.moving.x != 0)
            {             
                if (absVelX < maxVelocity.x)
                {
                    forceX = speed * controller.moving.x;
                    if (forceX < 0)
                    {
                        transform.localScale = new Vector3(-1, 1, 1);
                        facingRight = false;
                    }

                    else if (forceX > 0)
                    {
                        transform.localScale = new Vector3(1, 1, 1);
                        facingRight = true;
                    }                 
                }
            }

            if (controller.moving.y != 0)
            {
                if (absVelY < maxVelocity.y)
                    forceY = speed * controller.moving.y;
            }

            if (controller.moving.x != 0 || controller.moving.y != 0)
                anim.SetBool("isMoving", true);
            else
                anim.SetBool("isMoving", false);

            if (controller.shooting)
                shoot();
        }

        if (!isAlive)
            die();
    }

    void FixedUpdate()
    {
        rb2d.AddForce(new Vector2(forceX, forceY));

        if ((transform.position.x <= 0f) && rb2d.velocity.x < 0f)
        {
            rb2d.velocity = new Vector3(-rb2d.velocity.x, rb2d.velocity.y, 0);
        }

        if ((transform.position.x >= maxX) && rb2d.velocity.x > 0)
        {
            rb2d.velocity = new Vector3(-rb2d.velocity.x, rb2d.velocity.y, 0);
        }

        if ((transform.position.y <= minY) && rb2d.velocity.y < 0)
        {
            rb2d.velocity = new Vector3(rb2d.velocity.x, -rb2d.velocity.y, 0);
        }


    }

    void shoot()
    {
        GameObject currentBullet;
        currentBullet = Instantiate(bullet, this.transform.position, this.transform.rotation) as GameObject;

        if (facingRight)
            currentBullet.GetComponent<Rigidbody2D>().velocity = Vector3.right * bulletSpeed;
        else
            currentBullet.GetComponent<Rigidbody2D>().velocity = Vector3.right * -bulletSpeed;

        Destroy(currentBullet, 1);
    }

    void die()
    {
        Debug.Log("died");
        Application.LoadLevel(Application.loadedLevel);
    }

    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.gameObject.tag == "Wall")
        {
            Debug.Log("crashed");
            die();
        }

        if (target.gameObject.tag == "EnemyBullet")
        {
            die();
        }
    }
}
