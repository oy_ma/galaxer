﻿using UnityEngine;
using System.Collections;

public class LevelExit : MonoBehaviour {

	public static bool canExit;
    public AudioClip victoryClip;
    private Player player;

	// Use this for initialization
	void Start ()
    {
		canExit = false;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.gameObject.tag == "Player" && canExit) {
			Debug.Log ("can exit");
            player.isControllable = false;
            SoundManager.instance.PlaySingle(victoryClip);
            Invoke("LoadNext", victoryClip.length);           
        } 
		else if (target.gameObject.tag == "Player" && !canExit)
		{        
			Debug.Log("cannot exit");
		}
    }

    void LoadNext()
    {
        Application.LoadLevel(0);
        switch (Application.loadedLevel)
        {
            case 1:
                Application.LoadLevel(2);
                break;
            case 2:
                Application.LoadLevel(3);
                break;
            case 3:
                Application.LoadLevel(0);
                break;
        }
    }
}
