﻿using UnityEngine;
using System.Collections;

public class CameraPixelPerfect : MonoBehaviour {

    public float ppu = 18f;
    public float scale = 1f;
    public Vector2 nativeRes = new Vector2(240, 160);

	// Use this for initialization
	void Awake ()
    {
        var camera = GetComponent<Camera>();
        if (camera.orthographic)
        {
            scale *= Screen.height / nativeRes.y;
            ppu *= scale;
            camera.orthographicSize = (Screen.height / 2.0f) / ppu;
        }
	}
}
