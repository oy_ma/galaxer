﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {


    public float minX;
    public float maxX;
    public float minY;
    public float maxY;
    public GameObject target;
    private Transform t;
    private Vector3 velocity = Vector3.zero;

	// Use this for initialization
	void Start () {
       
        target = GameObject.FindGameObjectWithTag("Player");
        t = target.transform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate()
    {
        Vector3 pos = Vector3.SmoothDamp(transform.position, t.position, ref velocity, 0.2f);

        var cameraPosition = new Vector3(Mathf.Clamp(pos.x, minX, maxX), Mathf.Clamp(pos.y, minY, maxY), -10);

        transform.position = cameraPosition;

    }
}
