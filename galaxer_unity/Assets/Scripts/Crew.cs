﻿using UnityEngine;
using System.Collections;

public class Crew : MonoBehaviour {

    private static bool hasCpt;
    private static bool hasEng;
    private static bool hasMec;
    private TextBox textBox; 
	

	// Use this for initialization
	void Start () {
        GameObject obj = GameObject.Find("TextBoxHandler");
        textBox = obj.GetComponent<TextBox>();     
        hasCpt = false;
        hasEng = false;
        hasMec = false;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.gameObject.tag == "Player")
        {
            var crew = this.gameObject.tag;

            switch (crew)
            {
                case "Crew_Cpt":
                    textBox.ShowBox(0);
                    Debug.Log("got captain");
                    hasCpt = true;
                    if (hasCpt && hasEng && hasMec)
                    {
                        Debug.Log("got all");
                        LevelExit.canExit = true;
                    }                      
                    Destroy(gameObject);
                    break;
                case "Crew_Eng":
                    Debug.Log("got engineer");
                    hasEng = true;
                    if (hasCpt && hasEng && hasMec)
                    {
                        Debug.Log("got all");
                        LevelExit.canExit = true;
                    }                    
                    Destroy(gameObject);
                    break;
                case "Crew_Mec":
                    Debug.Log("got mechanic");
                    hasMec = true;
                    if (hasCpt && hasEng && hasMec)
                    {
                        Debug.Log("got all");
                        LevelExit.canExit = true;
                    }                   
                    Destroy(gameObject);
                    break;
            }
        }
    }
}
