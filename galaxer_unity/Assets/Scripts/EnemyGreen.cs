﻿using UnityEngine;
using System.Collections;

public class EnemyGreen : MonoBehaviour
{

    private float fireDistance = 5f;
    private float fireRate = 2f;
    private float fireTimer;
    private bool canFire = true;
    private Player player;
    public GameObject bullet;
    public float bulletSpeed = 3f;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        fireTimer += Time.deltaTime;

        if (fireTimer > fireRate)
            canFire = true;

        if (Vector3.Distance(transform.position, player.transform.position) < fireDistance && canFire)
        {
            Shoot();
            canFire = false;
        }
    }

    void Shoot()
    {
        GameObject currentBullet;
        currentBullet = Instantiate(bullet, this.transform.position, this.transform.rotation) as GameObject;
        Vector2 direction = player.transform.position - transform.position;
        direction.Normalize();
        currentBullet.GetComponent<Rigidbody2D>().velocity = direction * bulletSpeed;
        Destroy(currentBullet, 1.7f);
        fireTimer = 0;
    }

    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.gameObject.tag == "Bullet")
        {
            Debug.Log("enemy got shot");
            Destroy(gameObject);
        }
        else if (target.gameObject.tag == "Player")
        {
            Debug.Log("player got killed");
            player.isAlive = false;
        }
    }
}
