﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class Timer : MonoBehaviour {

    public Text timerText;
    public float timeLeft;
	private Player player;
	//GameObject p;

	// Use this for initialization
	void Start ()
    {
        GameObject canvas = GameObject.Find("UI");
        Text[] textValue = canvas.GetComponentsInChildren<Text>();
        timerText = textValue[0];

        timeLeft = 90f;

		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<Player>();

	


	}
	
	// Update is called once per frame
	void Update ()
    {

        timeLeft -= Time.deltaTime;
        timerText.text = "Time: " + FormatTime(timeLeft);

        if (timeLeft <= 0 && player.isAlive)
        {
            Debug.Log("time's up");
            player.isAlive = false;  
        }
	}

    string FormatTime(float value)
    {
        TimeSpan t = TimeSpan.FromSeconds(value);
        return string.Format("{0:D2}:{1:D2}", t.Minutes, t.Seconds);
    }
}
