﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

    private string[] buttons = new string[2] { "Start Game", "Quit" };   
    private int btnPosX = (Screen.width / 2) - 80;
    private int btnPosY = (Screen.height / 2) + 40;
    private int selected;
    private bool guiEnabled;

    GUIStyle gStyle;


    // Use this for initialization
    void Start ()
    {
        guiEnabled = false;
        selected = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
            selected = menuSelection(buttons, selected, "up");
        if (Input.GetKeyDown(KeyCode.DownArrow))
            selected = menuSelection(buttons, selected, "down");	
	}

    void OnGUI()
    {
        if (guiEnabled)
        {
            GUI.skin.button.hover.textColor = Color.green;
            GUI.backgroundColor = Color.clear;
            //GUI.skin.button.hover.background = null;

            GUI.SetNextControlName(buttons[0]);

            if (GUI.Button(new Rect(btnPosX, btnPosY, 160, 30), buttons[0]))
            {
                Debug.Log("start game");
                Application.LoadLevel(1);
            }

            GUI.SetNextControlName(buttons[1]);

            if (GUI.Button(new Rect(btnPosX, btnPosY + 30, 160, 30), buttons[1]))
            {
                Debug.Log("quit game");
                Application.Quit();
            }

            GUI.FocusControl(buttons[selected]);
        }       
    }

    int menuSelection(string[] buttonsArray, int selectedItem, string direction)
    {       
        if (direction == "up")
        {
            if (selectedItem == 0)
                selectedItem = buttonsArray.Length - 1;
            else
                selectedItem -= 1;
        }
        if (direction == "down")
        {
            if (selectedItem == buttonsArray.Length - 1)
                selectedItem = 0;
            else
                selectedItem += 1;         
        }
        return selectedItem;
    }

    void showGUI()
    {
        guiEnabled = true;
    }


}
