﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextBox : MonoBehaviour {
   
    private string[] text = {"Hey, thanks!"};

    public GameObject textBox;
    public Player player;
    public Text displayedText;

	void Start ()
    {
        textBox.SetActive(false);
        player = FindObjectOfType<Player>();

    }
	
    // displays textbox
    public void ShowBox(int id)
    {
        textBox.SetActive(true);
        displayedText.text = text[id];
        
        Invoke("RemoveBox", 3);
    }

    void RemoveBox()
    {
        textBox.SetActive(false);
    }
}
